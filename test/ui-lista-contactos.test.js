import { html, fixture, assert, fixtureCleanup } from '@open-wc/testing';
import '../ui-lista-contactos.js';

suite('<ui-lista-contactos>', () => {
  let el;

  teardown(() => fixtureCleanup());

  setup(async () => {
    el = await fixture(html`<ui-lista-contactos></ui-lista-contactos>`);
    await el.updateComplete;
  });

  test('instantiating the element with default properties works', () => {
    const element = el.shadowRoot.querySelector('p');
    assert.equal(element.innerText, 'Welcome to Cells');
  });

});





