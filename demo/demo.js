import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import './css/demo-styles.js';
import '../ui-lista-contactos.js';

export { mockData} from'./mock.js';

// Include below here your components only for demo
// import 'other-component.js'
