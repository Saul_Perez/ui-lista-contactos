import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import '@bbva-web-components/bbva-list-contact';
import styles from './UiListaContactos-styles.js';
/**

##styling-doc

@customElement ui-lista-contactos
@polymer
@LitElement
@demo demo/index.html
*/
export class UiListaContactos extends LitElement {
  static get is() {
    return 'ui-lista-contactos';
  }

  // Declare properties
  static get properties() {
    return {
      listaContactos: 
      {
        type: Array,
        attribute: 'item-list',
      },
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.listaContactos=[];
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('ui-lista-contactos-shared-styles')
    ]
  }

  _handleClick(evt) {
    const { item } = evt.target;
    console.log(evt);
    this.dispatchEvent(new CustomEvent('item-click', {
      bubbles: true,
      detail: item
    }));
  }

  // Define a template
  render() {
    return html` 
      ${this.listaContactos.map(item => html`
        <bbva-list-contact
        @list-contact-link-click="${ this._handleClick }
        
        >
          ${item.alias}
          <span slot="info">${item.person.birthDay}</span>
          <span slot="extra">${item.person.identityDocuments[0].country.name}</span>
        </bbva-list-contact>
      `)}
    `;
  }
}
